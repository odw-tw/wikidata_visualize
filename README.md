
# wikidata with force directed graph

Author : Chih-Kai Lu

## Introduction
這份文件主要說明使用d3.js視覺化wikidata資料的過程，並簡介wikidata

---

## Requirements
* OS : windows10
* srever : XAMPP -> environment for test
* python 3.x
    * package : [SPARQLWrapper](https://rdflib.github.io/sparqlwrapper/) 1.8.4 

---

## Wikidata

### introduction (wikidata spo)
Most of you might be heard of wikipeadia or even have used it, but wikidata seems less mentioned because wikipedia is for human but wikidata is for machine reading. 
The format stored is called structured data which is constructed by "SPO" (subject, predicate and object). "S", which means subject, and "O" are both identified by Qname which begins with the letter 'Q' followed by numbers, ex. [Q865](https://www.wikidata.org/wiki/Q865). "P", predicate, is identified by the letter 'P' followed by numbers, ex.  [P31](https://www.wikidata.org/wiki/Property:P31) . 


(OPTIONAL) if you want to deep into triple (SPO), read the [RDF1.1](https://www.w3.org/TR/rdf11-primer/) for more informaiton.

### property 
將資料轉換成圖形(graph)後，其中的連線(edge)就是屬性(property)，意即，想找到資料間的關聯，必須透過property，然而在wikidata裡編寫的時候，每個人對於property定義也許會略有不同，以triple方式舉例
> 李白(Q7071)  語言(**P1412**) 文言文(Q37041)
> 柳宗元(Q337773) 語言(**P1412**) 中文(Q7850)

適合延伸的property莫過於階層式屬性(hierarchical property),ex. supclass, instance of 

---

## [d3.js](https://d3js.org)
* prerequisite : html, css, javascript
### first step

* [tutorial](https://www.tutorialsteacher.com/d3js/loading-data-from-file-in-d3js) 幫助理解d3.js粗淺的概念和語法 

* [d3 document](https://github.com/d3/d3/wiki) 看code時有用

* [D3 gallery](https://github.com/d3/d3/wiki/Gallery) 裡面有相當多樣的圖形實作可供參考

### data preprocessing (with python)

下圖為使用 [wikidata query service](https://query.wikidata.org/) 抓取資料的SPARQL語法，可將下方程式碼複製到網站上試試看。抓的內容是"Parent taxons of Blue Whale"，[wikidata graph builder](https://angryloki.github.io/wikidata-graph-builder)的第一個範列。

```sql
SELECT ?item ?itemLabel ?linkTo {
  wd:Q42196 wdt:P171* ?item   # find the object that is the property p171 of Q42196
  OPTIONAL { ?item wdt:P171 ?linkTo }
  SERVICE wikibase:label {bd:serviceParam wikibase:language "en" }
}
# wd:Q42196 -> subject
# wdt:P171 -> predicate 
# ?item -> object
# The "*" sign behind P171 means to find P171 of ?item util the origin of Q42196. 
```

![](https://i.imgur.com/a6EOWnY.png)



下方JSON檔是"SparQL_getdata.py"使用SPARQLWrapper這個package，以類似wikidata query service使用SPARQL，抓下的原始JSON檔。我們所需要的資料為['results']['bindings']中的'item', 'itemLabel', 'linkTo'，
'item'裡的 'type', 'value'分別紀錄item的類型(uri)和值(wikidata中的網址)
'itemLabel'裡的 'xml:lang', 'type', 'value'分別紀錄此item在為json的語言(英文),類型(文字)和值(通稱)
'linkTo'裡的 'type', 'value', 分別紀錄此item所指向另一item(根據property)的類型(uri)和值(wikidata中的網址)
```json
{'head': {'vars': ['item', 'itemLabel', 'linkTo']}, 'results': {'bindings': [{'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetacea'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7993757'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetacea'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q729'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q19088'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'animal'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5173'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q729'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Bilateria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7377'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q19159'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'mammal'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q10915'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q150866'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Chordata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q19088'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2382443'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'eukaryote'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q19159'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26214'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Tetrapoda'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q19159'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q1209254'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Tetrapoda'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25241'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q10915'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Vertebrata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25241'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q84149'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Vertebrata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25329'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7377'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Artiodactyla'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25329'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Artiodactyla'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25329'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27379'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Artiodactyla'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q130942'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Placentalia'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q17092469'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Placentalia'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26214'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25241'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Gnathostomata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27207'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26214'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Osteichthyes'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27207'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q134681'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Osteichthyes'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27379'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q132666'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Laurasiatheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27850'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27379'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetartiodactyla'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27850'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5414620'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetartiodactyla'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q42196'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q133320'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'blue whale'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q84149'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q3280581'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Craniata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q130942'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7377'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Theria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q130942'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2478975'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Theria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q130942'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2910821'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Theria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q132666'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Boreoeutheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q132666'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q133342'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Boreoeutheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q133320'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q232829'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Balaenoptera'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q133342'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25833'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Exafroplacentalia'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q134681'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26214'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Teleostomi'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q150598'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160830'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Rhipidistia'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q150866'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5173'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'deuterostome'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160830'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25241'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Sarcopterygii'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160830'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27207'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Sarcopterygii'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q168366'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Mysticeti'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q232829'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q168366'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Balaenopteridae'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q727029'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q1369337'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetancodontamorpha'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q766050'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2141770'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cladotheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q1209254'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q150598'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Tetrapodomorph'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q1209254'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q23809240'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Tetrapodomorph'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q1369337'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q3428352'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Cetruminantia'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2141770'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2478975'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Trechnotheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2141770'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2513125'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Trechnotheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2382443'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'biota'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2478975'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2513125'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Holotheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2497754'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q766050'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Zatheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2513125'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26708524'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Theriiformes'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2910821'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q766050'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Boreosphenida'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2910821'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q2497754'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Boreosphenida'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q3280581'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q10915'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Olfactores'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q3428352'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27850'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Artiofabula'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5414620'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5444079'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Euungulata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q5444079'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7439311'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Fereuungulata'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7439311'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q27379'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Scrotifera'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7993757'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q25329'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Whippomorpha'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7993757'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q727029'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Whippomorpha'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q17092469'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q130942'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Eutheria'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q23809240'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q160830'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Dipnotetrapodomorpha'}}, {'item': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q26708524'}, 'linkTo': {'type': 'uri', 'value': 'http://www.wikidata.org/entity/Q7377'}, 'itemLabel': {'xml:lang': 'en', 'type': 'literal', 'value': 'Theriamorpha'}}]}}
```

用"SparQL_getdata.py"最後處理成檔名為"bluewhale.json"的JSON檔，分為 links 和 nodes 兩個key，

links用來表示圖形中點之間的連線，其中'source','target'用來表示由source連到target的線段，也就是原始json資料['item']['value']和['linkTo']['value'], 其值(value)的數字來自網址後綴的Qname。

nodes表示圖形中所代表的點，其中的'name'和'id'表示該點的名稱和Qname, 分別對應到['itemLabel']['value']和['item']['value'],

```json
{
  'links': [
     {'source': 160, 'target': 25833}, 
     {'source': 160, 'target': 7993757}, 
     {'source': 729, 'target': 19088}, {'source': 5173, 'target': 729}, {'source': 7377, 'target': 19159}, {'source': 10915, 'target': 150866}, {'source': 19088, 'target': 2382443}, {'source': 19159, 'target': 26214}, {'source': 19159, 'target': 1209254}, {'source': 25241, 'target': 10915}, {'source': 25241, 'target': 84149}, {'source': 25329, 'target': 7377}, {'source': 25329, 'target': 25833}, {'source': 25329, 'target': 27379}, {'source': 25833, 'target': 130942}, {'source': 25833, 'target': 17092469}, {'source': 26214, 'target': 25241}, {'source': 27207, 'target': 26214}, {'source': 27207, 'target': 134681}, {'source': 27379, 'target': 132666}, {'source': 27850, 'target': 27379}, {'source': 27850, 'target': 5414620}, {'source': 42196, 'target': 133320}, {'source': 84149, 'target': 3280581}, {'source': 130942, 'target': 7377}, {'source': 130942, 'target': 2478975}, {'source': 130942, 'target': 2910821}, {'source': 132666, 'target': 25833}, {'source': 132666, 'target': 133342}, {'source': 133320, 'target': 232829}, {'source': 133342, 'target': 25833}, {'source': 134681, 'target': 26214}, {'source': 150598, 'target': 160830}, {'source': 150866, 'target': 5173}, {'source': 160830, 'target': 25241}, {'source': 160830, 'target': 27207}, {'source': 168366, 'target': 160}, {'source': 232829, 'target': 168366}, {'source': 727029, 'target': 1369337}, {'source': 766050, 'target': 2141770}, {'source': 1209254, 'target': 150598}, {'source': 1209254, 'target': 23809240}, {'source': 1369337, 'target': 3428352}, {'source': 2141770, 'target': 2478975}, {'source': 2141770, 'target': 2513125}, {'source': 2382443}, {'source': 2478975, 'target': 2513125}, {'source': 2497754, 'target': 766050}, {'source': 2513125, 'target': 26708524}, {'source': 2910821, 'target': 766050}, {'source': 2910821, 'target': 2497754}, {'source': 3280581, 'target': 10915}, {'source': 3428352, 'target': 27850}, {'source': 5414620, 'target': 5444079}, {'source': 5444079, 'target': 7439311}, {'source': 7439311, 'target': 27379}, {'source': 7993757, 'target': 25329}, {'source': 7993757, 'target': 727029}, {'source': 17092469, 'target': 130942}, {'source': 23809240, 'target': 160830}, {'source': 26708524, 'target': 7377}
    ], 
  'nodes': [
    {'name': 'Cetacea', 'id': 160}, 
    {'name': 'animal', 'id': 729}, 
    {'name': 'Bilateria', 'id': 5173}, {'name': 'mammal', 'id': 7377}, {'name': 'Chordata', 'id': 10915}, {'name': 'eukaryote', 'id': 19088}, {'name': 'Tetrapoda', 'id': 19159}, {'name': 'Vertebrata', 'id': 25241}, {'name': 'Artiodactyla', 'id': 25329}, {'name': 'Placentalia', 'id': 25833}, {'name': 'Gnathostomata', 'id': 26214}, {'name': 'Osteichthyes', 'id': 27207}, {'name': 'Laurasiatheria', 'id': 27379}, {'name': 'Cetartiodactyla', 'id': 27850}, {'name': 'blue whale', 'id': 42196}, {'name': 'Craniata', 'id': 84149}, {'name': 'Theria', 'id': 130942}, {'name': 'Boreoeutheria', 'id': 132666}, {'name': 'Balaenoptera', 'id': 133320}, {'name': 'Exafroplacentalia', 'id': 133342}, {'name': 'Teleostomi', 'id': 134681}, {'name': 'Rhipidistia', 'id': 150598}, {'name': 'deuterostome', 'id': 150866}, {'name': 'Sarcopterygii', 'id': 160830}, {'name': 'Mysticeti', 'id': 168366}, {'name': 'Balaenopteridae', 'id': 232829}, {'name': 'Cetancodontamorpha', 'id': 727029}, {'name': 'Cladotheria', 'id': 766050}, {'name': 'Tetrapodomorph', 'id': 1209254}, {'name': 'Cetruminantia', 'id': 1369337}, {'name': 'Trechnotheria', 'id': 2141770}, {'name': 'biota', 'id': 2382443}, {'name': 'Holotheria', 'id': 2478975}, {'name': 'Zatheria', 'id': 2497754}, {'name': 'Theriiformes', 'id': 2513125}, {'name': 'Boreosphenida', 'id': 2910821}, {'name': 'Olfactores', 'id': 3280581}, {'name': 'Artiofabula', 'id': 3428352}, {'name': 'Euungulata', 'id': 5414620}, {'name': 'Fereuungulata', 'id': 5444079}, {'name': 'Scrotifera', 'id': 7439311}, {'name': 'Whippomorpha', 'id': 7993757}, {'name': 'Eutheria', 'id': 17092469}, {'name': 'Dipnotetrapodomorpha', 'id': 23809240}, {'name': 'Theriamorpha', 'id': 26708524}
   ]
}
```

> 這次的實作中，筆者過於執著於使用 python ，事後回想，使用 javascript 直接抓資料或許更好，不必兩種程式切換，而 wikidata query service 下面也確實有提供 javascript 的 code

### force directed graph



* [d3-force function ](https://github.com/d3/d3-force#simulation_fix)  介紹d3-force的document

* [basic network](https://www.d3-graph-gallery.com/graph/network_basic.html) : 簡單的示範，可以幫助理解


* [Force Dragging I](https://bl.ocks.org/mbostock/2675ff61ea5e063ede2b5d63c08020c7) : 這次code的主體

* [D3 Mouse Events](http://bl.ocks.org/WilliamQLiu/76ae20060e19bf42d774) : 參考 mouseover 和 mouseout 兩種event的實作，

```
須注意
d3.select(this).attr({
              fill: "orange",
              r: radius * 2
            });
            
attr設置r和color的部分須拆開，用style或attr設定

  d3.select(this)
    .style("fill",orange)
    .attr("r",radius*2);
    
```

* [Simple d3.js tooltips](http://bl.ocks.org/d3noob/a22c42db65eb00d4e369) : 參考如何顯示label

* [Adding links to objects](http://bl.ocks.org/d3noob/8150631) : 為物件加入連結功能的方法

## Usage 
1. 使用"SparQL_getdata.py"轉出"bluewhale.json"
2. 開啟XAMPP,將"d3_visualize.html"放置到htdoc資料夾("bluewhale.json"也要在同一個目錄下)
3. 瀏覽器輸入"localhost/d3_visualize.html" 

下圖為最後實作結果，游標移至點上會顯示出該點的資料，點擊後會在新分頁中開啟該資料於wikidata中的網址

![](https://i.imgur.com/bkex9CN.png)