from SPARQLWrapper import SPARQLWrapper, JSON
import json
import re
import pandas as pd


# get the Qname id
def getQname(url):
    qname = re.sub(r'.+Q', "", url)
    qname = int(qname)
    return qname


# delete duplicated items
def duplicate(items):
    unique = []
    for item in items:
        if item not in unique:
            unique.append(item)
    return unique


sparql = SPARQLWrapper("https://query.wikidata.org/sparql")
# From https://www.wikidata.org/wiki/Wikidata:SPARQL_query_service/queries/examples#Cats
sparql.setQuery("""
SELECT ?item ?itemLabel ?linkTo {
  wd:Q42196 wdt:P171* ?item
  OPTIONAL{?item wdt:P171 ?linkTo}
  SERVICE wikibase:label {bd:serviceParam wikibase:language "en" }
}
""")

sparql.setReturnFormat(JSON)
results = sparql.query().convert()
# print(type(results)) <class 'dict'>
# print(results['results']['bindings'])
result_bindings = results['results']['bindings']

# make list of links
links = []
for e in result_bindings:
    dic = {}
    dic['source'] = getQname(e['item']['value'])
    if 'linkTo' in e:
        dic['target'] = getQname(e['linkTo']['value'])
        if not (dic['target']):
            continue # if the node has no target node, do not put the link into "links"
    links.append(dic)

# make list of nodes
b = []
for e in result_bindings:
    dic = {}
    dic['name'] = e['itemLabel']['value']
    dic['id'] = getQname(e['item']['value'])
    b.append(dic)
# remove duplicated nodes
b = duplicate(b)
# print(b)

# dictionary data_node_link with two keys "links" and "nodes"
data_node_link = {}
data_node_link['links'] = links
data_node_link['nodes'] = b
# print(data_node_link)

json_out = open("bluewhale.json", 'w')
data_node_link = json.dumps(data_node_link)  # convert to type of str
json_out.write(data_node_link)

json_out.close()

# print(getQname(data_node_link['links'][0]['source']))
